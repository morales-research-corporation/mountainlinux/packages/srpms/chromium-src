// Copyright 2020 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// https://github.com/WICG/storage-buckets

[
  Exposed=(Window,Worker),
  RuntimeEnabled=StorageBuckets,
  SecureContext
] interface BucketManager {
    [CallWith=ScriptState, RaisesException] Promise<USVString> openOrCreate(USVString name);
    [CallWith=ScriptState, RaisesException] Promise<sequence<USVString>> keys();
    [CallWith=ScriptState, RaisesException, ImplementedAs=Delete] Promise<void> delete(USVString name);
};
