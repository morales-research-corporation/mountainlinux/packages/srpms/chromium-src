// Copyright 2020 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package chromeos.phonehub.proto;

option optimize_for = LITE_RUNTIME;

// This enum is tied directly to a UMA enum defined in
// //tools/metrics/histograms/enums.xml, and should always reflect it (do not
// change one without changing the other). Entries should be never modified
// or deleted. Only additions possible.
enum MessageType {
  PROVIDE_CROS_STATE = 0;
  PHONE_STATUS_SNAPSHOT = 1;
  PHONE_STATUS_UPDATE = 2;
  UPDATE_NOTIFICATION_MODE_REQUEST = 3;
  UPDATE_NOTIFICATION_MODE_RESPONSE = 4;
  RING_DEVICE_REQUEST = 5;
  RING_DEVICE_RESPONSE = 6;
  UPDATE_BATTERY_MODE_REQUEST = 7;
  UPDATE_BATTERY_MODE_RESPONSE = 8;
  DISMISS_NOTIFICATION_REQUEST = 9;
  DISMISS_NOTIFICATION_RESPONSE = 10;
  NOTIFICATION_INLINE_REPLY_REQUEST = 11;
  NOTIFICATION_INLINE_REPLY_RESPONSE = 12;
  SHOW_NOTIFICATION_ACCESS_SETUP_REQUEST = 13;
  SHOW_NOTIFICATION_ACCESS_SETUP_RESPONSE = 14;
}

enum NotificationSetting {
  NOTIFICATIONS_OFF = 0;
  NOTIFICATIONS_ON = 1;
}

enum ChargingState {
  NOT_CHARGING = 0;
  CHARGING_AC = 1;
  CHARGING_USB = 2;
  CHARGING_WIRELESS = 3;
}

enum BatteryMode {
  BATTERY_SAVER_OFF = 0;
  BATTERY_SAVER_ON = 1;
}

enum NotificationMode {
  DO_NOT_DISTURB_OFF = 0;
  DO_NOT_DISTURB_ON = 1;
}

enum NotificationAccessState {
  ACCESS_NOT_GRANTED = 0;
  ACCESS_GRANTED = 1;
}

enum FindMyDeviceRingStatus {
  NOT_RINGING = 0;
  RINGING = 1;
}

enum SignalStrength {
  ZERO_BARS = 0;
  ONE_BAR = 1;
  TWO_BARS = 2;
  THREE_BARS = 3;
  FOUR_BARS = 4;
}

enum MobileConnectionState {
  NO_SIM = 0;
  SIM_BUT_NO_RECEPTION = 1;
  SIM_WITH_RECEPTION = 2;
}

enum NotificationImportance {
  UNSPECIFIED = 0;
  NONE = 1;
  MIN = 2;
  LOW = 3;
  DEFAULT = 4;
  HIGH = 5;
}

enum ProfileType {
  DEFAULT_PROFILE = 0;
  WORK_PROFILE = 1;
}

enum FindMyDeviceCapability {
  NORMAL = 0;
  NOT_ALLOWED = 1;
}

message PhoneProperties {
  int32 battery_percentage = 1;
  ChargingState charging_state = 2;
  BatteryMode battery_mode = 3;

  // Note: If |connection_state| is not SIM_WITH_RECEPTION,
  // |signal_strength| and |mobile_provider| should be ignored.
  MobileConnectionState connection_state = 4;
  SignalStrength signal_strength = 5;
  string mobile_provider = 6;

  NotificationMode notification_mode = 7;
  NotificationAccessState notification_access_state = 8;

  FindMyDeviceRingStatus ring_status = 9;

  ProfileType profile_type = 10;

  FindMyDeviceCapability find_my_device_capability = 11;
}

message App {
  string package_name = 1;
  string visible_name = 2;
  bytes icon = 3;
}

message CrosState {
  NotificationSetting notification_setting = 1;
}

message Action {
  enum InputType {
    UNKNOWN = 0;
    TEXT = 1;
    CONFIRMATION = 2;
    OPEN = 3;
    CANNED_REPLY = 4;
  }

  int64 id = 1;
  string title = 2;
  // Optional, but not specifying a type implies no response can be sent.
  InputType type = 3;
}

message Notification {
  int64 id = 1;
  int64 epoch_time_millis = 2;
  App origin_app = 3;
  NotificationImportance importance = 4;
  string title = 5;
  string text_content = 6;
  repeated Action actions = 7;

  // Optionals:
  bytes contact_image = 8;     // for messages
  bytes background_image = 9;  // for media
  bytes shared_image = 10;     // for messages
}

message PhoneStatusSnapshot {
  PhoneProperties properties = 1;
  repeated Notification notifications = 2;
}

message PhoneStatusUpdate {
  PhoneProperties properties = 1;
  // Notifications which have changed since the last update.
  repeated Notification updated_notifications = 2;
  // IDs of notification which have been removed since the last update.
  repeated int64 removed_notification_ids = 3;
}

message UpdateNotificationModeRequest {
  NotificationMode notification_mode = 1;
}

message UpdateNotificationModeResponse {}

message RingDeviceRequest {
  FindMyDeviceRingStatus ring_status = 1;
}

message RingDeviceResponse {}

message UpdateBatteryModeRequest {
  BatteryMode battery_mode = 1;
}

message UpdateBatteryModeResponse {}

message DismissNotificationRequest {
  int64 notification_id = 1;
}

message DismissNotificationResponse {}

message NotificationInlineReplyRequest {
  int64 notification_id = 1;
  string reply_text = 2;
}

message NotificationInlineReplyResponse {}

message ShowNotificationAccessSetupRequest {}

message ShowNotificationAccessSetupResponse {}
