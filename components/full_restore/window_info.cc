// Copyright 2020 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "components/full_restore/window_info.h"

namespace full_restore {

WindowInfo::WindowInfo() = default;
WindowInfo::~WindowInfo() = default;

}  // namespace full_restore
