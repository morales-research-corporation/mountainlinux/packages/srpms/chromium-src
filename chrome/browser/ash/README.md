chrome/browser/ash
==================

This directory should contain non-UI Chrome OS specific code that has
`chrome/browser` dependencies.

As of January 2021, code from
[`chrome/browser/chromeos`](/chrome/browser/chromeos/README.md) is migrating
into this directory, as part of the [Lacros project](/docs/lacros.md).

Googlers: See go/lacros-directory-migration for more details.
